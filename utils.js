
const fs = require('fs')
const data = require('./data/employee.js')
class ErrorFile {

    static notArray = new Error("Input MissMatch Excepted Array")
    static notString = new Error("Input MissMatch Expected String")
    static emptyObject = new Error("The Object is empty ")
    static errorInCallBack = new Error("CallBack function is not present or error at call back")
    static notObject = new Error("ExceptedObject -- Inut Miss Match")
    static pathNotFound = new Error("Path not found")
    static notNumber = new Error("not an number Input miss Match")
    static countMissMatch = new Error("count of elements miss match")

}

class FileHandler {

    static readDataFromFile(path, cb, option = 'utf-8') {
        try {
            if (typeof path != 'string') {
                throw ErrorFile.notString
            }
            if (typeof cb != 'function' || cb == null) {
                throw ErrorFile.errorInCallBack
            }
            fs.exists(path, (callback) => {
                if (callback) {
                    fs.readFile(path, option, (err, data) => {
                        cb(err, JSON.parse(data))
                    })
                } else {
                    cb(ErrorFile.pathNotFound, null)
                }
            })

        } catch (err) {
            cb(err, null)
        }
    }
    static writeDataToFile(path, data, cb, option = 'utf-8',) {
        try {
            if (cb == null || typeof cb != 'function') {
                console.log(cb)
                throw ErrorFile.errorInCallBack
            }
            if (typeof data != "object") {
                (console.log(typeof data))
                throw ErrorFile.notObject
            }
            if (typeof path != "string") {
                throw ErrorFile.notString
            }
            if (data.isEmpty) {
                throw ErrorFile.emptyObject
            }
            fs.writeFile(path, JSON.stringify(data), option, (err) => {

                let done = `data saved succfully at path ${path}`

                if (err) {
                    done = null
                    cb(err, done)
                }
                else {
                    console.log(data)
                    cb(err, done)
                }

            })
        } catch (error) {
            console.log(error.message)
        }
    }
}


class Paths {
    static dataFile = "data/employee.js"
    static employeeWithIdsFile = "output/employee_with_ids.json"
    static groupedEmployee = "output/group_data_baced_company.json"
    static dataOfCompany="output/company_data.json"
    static dataWithOutId = "output/data_with_out_id.json"
    static sorteredData = "output/sorted_data.json"
    static swapData = "output/swap_data.json"
    static evenEmployeeBirthdate = "output/even_employee_birthday.json"
}

class Data {
    static employeesData = require("./data/employee.js")
}

module.exports = {
    errrorFile: ErrorFile,
    FileHandler: FileHandler,
    Paths: Paths,
    Data: Data
}