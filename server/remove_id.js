// Remove entry with id 2.

// const { employees } = require("../data/employee");
const { errrorFile, Data, FileHandler, Paths } = require("../utils");

function removeEntry(id) {
    try {
        if (typeof id != 'number') {
            throw errrorFile.notNumber
        }
        let requiredData ={}
         requiredData['employees'] = Data.employeesData.employees.filter((employee) => employee['id'] != 2)
        FileHandler.writeDataToFile(Paths.dataWithOutId, requiredData, (err, message) => {
            if (err) {
                throw err
            }
            else {
                console.log(message)
            }
        })
    } catch (err) {
        console.log(err)
    }
}

removeEntry(2)