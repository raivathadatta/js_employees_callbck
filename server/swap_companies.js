const { employees } = require("../data/employee");
const { errrorFile, Data, FileHandler, Paths } = require("../utils");

// swap position of companies with id 93 and id 92.
function switchCompanies(...ids) {
    try {
        if(ids.length !=2){
            throw errrorFile.countMissMatch
        }
        console.log(ids)
        if (typeof ids[0] != 'number') {
            throw errrorFile.notNumber
        }
        if (typeof ids[1] != 'number') {
            throw errrorFile.notNumber
        }
        let indexs = []
        console.log((Data.employeesData))
        ids.forEach(id => {
            Data.employeesData.employees.forEach((element) => {
                if (element['id'] == id) {
                    indexs.push(element)
                }
            })
        });

        for (let index = 0; index < indexs.length; index++) {
            indexs[index] = Data.employeesData.employees.indexOf(indexs[index])
        }
        let outputData = Data.employeesData
        let companyName = outputData['employees'][indexs[0]]['company']
        outputData['employees'][indexs[0]]['company'] = outputData['employees'][indexs[1]]['company']
        outputData['employees'][indexs[1]]['company'] = companyName

        FileHandler.writeDataToFile(Paths.swapData, outputData, (err, message) => {
            if (err) {
                throw err
            } else {
                console.log((message))
            }
        })
    } catch (err) {
        console.log(err)
    }

}
switchCompanies(92, 93)