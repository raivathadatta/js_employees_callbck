
// 2. Group data based on companies.
// { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
const { Data, FileHandler, Paths } = require("../utils.js")


function groupDataBacedOnCompany() {
    try {
        let groupedEmplooyes = Data.employeesData.employees.reduce((accumilater, present) => {
            if (!accumilater[present['company']]) {
                accumilater[present['company']] = []
            }
            accumilater[present['company']].push(present)
            return accumilater
        }, {})
        FileHandler.writeDataToFile(Paths.groupedEmployee, groupedEmplooyes, (err, message) => {
            if (err) {
                throw err
            } else {
                console.log(message)
            }
        })
    } catch (err) {
        console.log(err)
    }
}

groupDataBacedOnCompany()