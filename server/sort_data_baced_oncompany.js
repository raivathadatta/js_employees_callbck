const { employees } = require("../data/employee");
const { Data, FileHandler, Paths } = require("../utils.js");

function sortData() {
    try {
        let sortedData = {}
        sortedData['employees'] = Data.employeesData.employees.sort(
            (employee1, employee2) => {
                if (employee1['company'] < employee2['company']) {
                    return -1
                } else {
                    if (employee1['company'] == employee2['company']) {
                        return employee1['id'] - employee2['id']
                    }
                }
            })
        FileHandler.writeDataToFile(Paths.sorteredData, sortedData, (err, message) => {
            if (err) {
                throw err

            } else {
                console.log(message)
            }
        })
    } catch (err) {
        console.log(err)
    }
}

sortData()