
const utils = require("../utils.js")
// 1. Retrieve data for ids : [2, 13, 23].

function getEmployeesBacedOnIds(...ids){
    try {
        if(Array.isArray(ids[0])){
            ids=ids.pop()
        }
        
        if(!Array.isArray(ids)){
            throw utils.errrorFile.notArray
        }
       let requireEmployees= utils.Data.employeesData.employees.filter((employee)=>ids.includes(employee['id']))
       utils.FileHandler.writeDataToFile(utils.Paths.employeeWithIdsFile,requireEmployees,(err,message)=>{
        if(err){
            throw err
        }
        console.log(message)
       })

    } catch (error) {
        console.log(error)
    }

}
getEmployeesBacedOnIds([12,13,23])