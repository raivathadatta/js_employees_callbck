//   7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

const { Data, FileHandler, Paths } = require("./utils")



function addBirthDaysToEvenEmployees() {
    try {
        let companiesData = {}
        companiesData['employees'] = Data.employeesData.employees.map((employee) => {
            if ((employee['id']) % 2 == 0) {
                employee['date_of_birth'] = new Date().toJSON().slice(0, 10);
            }
            return employee
        })
        console.log(Paths.evenEmployeeBirthdate)
        FileHandler.writeDataToFile(Paths.evenEmployeeBirthdate, companiesData, (err, message) => {
            if (err) {
                throw err
            }
            else {
                console.log(message)
            }
        })
    } catch (error) {
        console.log((error))
    }
}
addBirthDaysToEvenEmployees()